@component('mail::emailer')
<h1>Hey Joseph,</h1>

<p>Thanks for registering an account with Fuzook It. Please verify your account by clicking the button below.</p>

@component('mail::button', ['url' => 'http://fuzook.mydevwebsites.info'])
Verify my Email
@endcomponent

@endcomponent