<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;

use App\Mail\EmailerTemplate;

class SendEmailController extends Controller
{
    //
    public function send() {

        Mail::to("cassystem12@gmail.com")->send(new EmailerTemplate());

        return response()->json(['message' => 'sent']);

    }
}
